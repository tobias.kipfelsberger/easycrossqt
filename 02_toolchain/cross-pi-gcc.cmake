# project root folder
set(PROJ_ROOT ${CMAKE_CURRENT_LIST_DIR}/..)


# this one is important
SET (CMAKE_SYSTEM_NAME Linux)
# this one not so much
SET (CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
SET (CMAKE_C_COMPILER ${PROJ_ROOT}/02_toolchain/cross-pi-gcc/bin/arm-linux-gnueabihf-gcc)
SET (CMAKE_CXX_COMPILER ${PROJ_ROOT}/02_toolchain/cross-pi-gcc/bin/arm-linux-gnueabihf-g++)

# where is the target environment
SET (CMAKE_FIND_ROOT_PATH ${PROJ_ROOT}/03_sysroot)

# search for programs in the build host directories
SET (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)


# List of library dirs where LD has to look. Pass them directly through gcc. LD_LIBRARY_PATH is not evaluated by arm-*-ld
set(LIB_DIRS
	"${PROJ_ROOT}/03_sysroot/usr/local/lib"
	"${PROJ_ROOT}/03_sysroot/lib/arm-linux-gnueabihf"
	"${PROJ_ROOT}/03_sysroot/usr/lib"
)

# You can additionally check the linker paths if you add the flags ' -Xlinker --verbose'
set(COMMON_FLAGS "-I${PROJ_ROOT}/02_toolchain/cross-pi-gcc/include ")
FOREACH(LIB ${LIB_DIRS})
	set(COMMON_FLAGS "${COMMON_FLAGS} -L${LIB} -Wl,-rpath-link,${LIB}")
ENDFOREACH()

set(CMAKE_C_FLAGS "-mcpu=cortex-a72 -mtune=cortex-a72 -mfpu=neon-fp-armv8 -mfloat-abi=hard ${COMMON_FLAGS}" CACHE STRING "Flags for Raspberry PI 4")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "Flags for Raspberry PI 3")


