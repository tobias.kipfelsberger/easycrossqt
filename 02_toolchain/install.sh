#!/bin/bash

basedir=`dirname "$(realpath $0)"`

## toolchain obtained from https://github.com/abhiTronix/raspberry-pi-cross-compilers/wiki/Cross-Compiler:-Installation-Instructions

cd $basedir

#download crosstoolchain
wget -nc https://sourceforge.net/projects/raspberry-pi-cross-compilers/files/Raspberry%20Pi%20GCC%20Cross-Compiler%20Toolchains/Buster/GCC%209.2.0/Raspberry%20Pi%203A%2B%2C%203B%2B%2C%204/cross-gcc-9.2.0-pi_3%2B.tar.gz/download -O cross-gcc.tar.gz

#unpack
tar -xvzf cross-gcc.tar.gz

#remove the file again
rm -f cross-gcc.tar.gz

echo "don't forget to update the symlink and cmake toolchain file in case you updated!"
