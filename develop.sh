#!/bin/bash

basedir=`dirname "$(realpath $0)"`

# where are all temporary files being stored... this includes the build folder
TMP_DIR=$basedir/tmp
CODE_DIR=$basedir/10_code

# install folder of qtcreator
QTCREATOR_DIR=$basedir/01_tools/qtcreator
CMAKE_DIR=$basedir/01_tools/cmake

# name of the KIT
CROSS_KIT_NAME="raspikit"
CROSSCOMPILE_PREFIX=arm-linux-gnueabihf
TOOLCHAINROOT=$basedir/02_toolchain/cross-pi-gcc
TOOLCHAINFILE=$basedir/02_toolchain/cross-pi-gcc.cmake
SYSROOT=$basedir/03_sysroot

# device info
TARGET_IP="raspberrypi.local"
TARGET_USER="pi"
TARGET_PASS="raspberry"

# qt related info
QMAKE_PATH=$basedir/04_3rdparty/qt/install/host/bin/qmake
QT_CMAKE_PATH=$basedir/04_3rdparty/qt/install
# cmake related info
CMAKE_PATH=$basedir/01_tools/cmake/bin/cmake


checkinstalled()
{
    if ! which $1 >/dev/null; then
        echo "$1 is not installed"
        echo "-> 'apt-get install $2'"
        exit 1
    fi
}

printhelp()
{
    echo "Usage $0 [options]"
    echo " --embedded       build for embedded [default]"
    echo " --desktop        build for destop"
    echo " --clean          clean everything"
    echo " --dryrun         only print commands instead of execting them"
    echo " --ide            open project in IDE [default]; all commands below will be ignored"
    echo " --cmakegui       open project with cmake-gui"
    echo " --terminal       open project in terminal"
    echo " --makefiles      cmake will generate for Unix Makefiles"
    echo " --ninja          cmake will generate for Ninja [default]"
}

run_cmd() {
    if [ -n "$dryrun" ]; then
        echo "'$@'"
    elif [ "${SILENCE_ERRORS}" = 1 ]; then
        $@ &>/dev/null
    else
        "$@"
    fi
}

runsdktool() {
    run_cmd $QTCREATOR_DIR/libexec/qtcreator/sdktool ${@}
}

printline()
{
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
}

createMakeTerminal()
{
    MAKEFILE_CMD=$1
    WORKING_DIR=$2
    TEXT="
*************************************
* now call $MAKEFILE_CMD (install)
*************************************
"
    run_cmd x-terminal-emulator -e "cd $WORKING_DIR; echo \"$TEXT\"; bash"
}

setupCmakeGeneratorName()
{
    MAKEFILE_CMD=$1
    if [ "$MAKEFILE_CMD" == "ninja" ]; then
        CMAKE_GENERATOR_NAME="Ninja"
    elif [ "$MAKEFILE_CMD" == "make" ]; then
        CMAKE_GENERATOR_NAME="Unix Makefiles"
    else
        echo "cannot create cmake generator"
        exit 1
    fi
}

setupCmake()
{
    MAKEFILE_CMD=$1
    BUILD_TYPE=$2
    TMP_DIR=$3

    setupCmakeGeneratorName "$MAKEFILE_CMD"    

    echo ""
    printline
    echo "setup project for build type \"$BUILD_TYPE\" in directory \"$TMP_DIR\""
    printline
    echo ""

    # run cmake with given parameters first

    if [ "$embedded" == "1" ]; then
        run_cmd $CMAKE_PATH \
               -DCMAKE_VERBOSE_MAKEFILE=0 \
               -DCMAKE_PREFIX_PATH="$QT_CMAKE_PATH" \
               -G "$CMAKE_GENERATOR_NAME" \
               -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
               -S $CODE_DIR \
               -B $TMP_DIR \
               -DCMAKE_TOOLCHAIN_FILE="$TOOLCHAINFILE"
               
                
    else
        run_cmd $CMAKE_PATH \
               -DCMAKE_VERBOSE_MAKEFILE=0 \
               -DCMAKE_PREFIX_PATH="$QT_CMAKE_PATH" \
               -G "$CMAKE_GENERATOR_NAME" \
               -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
               -S $CODE_DIR \
               -B $TMP_DIR
    fi

    if [ "$cmakegui" == "1" ]; then
        run_cmd $CMAKE_DIR/bin/cmake-gui -S $CODE_DIR -B $TMP_DIR
    fi

    if [ "$terminal" == "1" ]; then
        createMakeTerminal "$MAKEFILE_CMD" "$TMP_DIR"
    fi
}

clean_project()
{
    echo "clean project"
    removekit_arm
    rm -rf $TMP_DIR
    rm -f $CODE_DIR/CMakeLists.txt.user
}

create_cmake()
{
    BUILD_DIR=$TMP_DIR/build
    
    #in case no IDE is involved, just create a extra folder containing the makefiles
    #by default we will create 'Release' builds.. this can be changed easily in the cmake-gui
    setupCmake $makecmd "Release" "$BUILD_DIR"
}

open_qtcreator()
{
    removekit_arm
    addkit_arm
    run_cmd $QTCREATOR_DIR/bin/qtcreator.sh $CODE_DIR
}

addkit_arm() 
{
    GCC="$TOOLCHAINROOT/bin/$CROSSCOMPILE_PREFIX-gcc"
    GPLUSPLUS="$TOOLCHAINROOT/bin/$CROSSCOMPILE_PREFIX-g++"
    GDB=`which gdb-multiarch`

    runsdktool addTC \
        --id "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-gcc" \
        --language 1 \
        --name "$CROSS_KIT_NAME-GCC" \
        --path "$GCC" \
        --abi arm-linux-generic-elf-32bit \
        --supportedAbis arm-linux-generic-elf-32bit,arm-linux-generic-elf-32bit

    runsdktool addTC \
        --id "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-g++" \
        --language 2 \
        --name "$CROSS_KIT_NAME-G++" \
        --path "$GPLUSPLUS" \
        --abi arm-linux-generic-elf-32bit \
        --supportedAbis arm-linux-generic-elf-32bit,arm-linux-generic-elf-32bit

    mkdir -p $TMP_DIR/gdb
    runsdktool addDebugger \
        --id "$CROSS_KIT_NAME-GDB" \
        --name "$CROSS_KIT_NAME-GDB" \
        --engine 1 \
        --binary "$GDB" \
        WorkingDirectory QString:$TMP_DIR/gdb

    runsdktool addDev \
        --id "$CROSS_KIT_NAME-Device" \
        --name "$CROSS_KIT_NAME-Device" \
        --freePorts 10000-10100 \
        --authentication 3 \
        --host "$TARGET_IP" \
        --origin "0" \
        --osType "GenericLinuxOsType" \
        --password "$TARGET_PASS" \
        --sshPort "22" \
        --timeout "180" \
        --type "0" \
        --uname "$TARGET_USER"

    runsdktool addQt \
        --id "$CROSS_KIT_NAME-Qt" \
        --name "$CROSS_KIT_NAME-Qt" \
        --qmake "$QMAKE_PATH" \
        --type "RemoteLinux.EmbeddedLinuxQt"

    runsdktool addCMake \
        --id "$CROSS_KIT_NAME-CMake" \
        --name "$CROSS_KIT_NAME-CMake" \
        --path "$CMAKE_PATH"

    runsdktool addKit \
        --id "$CROSS_KIT_NAME-Kit" \
        --name "$CROSS_KIT_NAME-Kit" \
        --debuggerid "$CROSS_KIT_NAME-GDB" \
        --devicetype GenericLinuxOsType \
        --device "$CROSS_KIT_NAME-Device" \
        --Ctoolchain "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-gcc" \
        --Cxxtoolchain "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-g++" \
        --qt "$CROSS_KIT_NAME-Qt" \
        --cmake "$CROSS_KIT_NAME-CMake" \
        --cmake-config "CMAKE_TOOLCHAIN_FILE:STRING=$TOOLCHAINFILE" \
        --cmake-generator "Ninja" \
        --cmake-config "CMAKE_PREFIX_PATH:PATH=$QT_CMAKE_PATH" \
        --cmake-config "QT_QMAKE_EXECUTABLE:STRING=%{Qt:qmakeExecutable}" \
        --cmake-config "CFG_GDBINIT_LOCATION:PATH=$workdir/.gdbinit"
}

dbg() {
    runsdktool addKit \
        --id "$CROSS_KIT_NAME-Kit" \
        --name "$CROSS_KIT_NAME-Kit" \
        --debuggerid "$CROSS_KIT_NAME-GDB" \
        --devicetype GenericLinuxOsType \
        --device "$CROSS_KIT_NAME-Device" \
        --sysroot $SYSROOT \
        --Ctoolchain "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-gcc" \
        --Cxxtoolchain "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-g++" \
        --qt "$CROSS_KIT_NAME-Qt" \
        --cmake "$CROSS_KIT_NAME-CMake" \
        --cmake-generator "Ninja" \
        --cmake-config "CMAKE_TOOLCHAIN_FILE:STRING=$TOOLCHAINFILE" \
        --cmake-config "CMAKE_C_COMPILER:STRING=%{Compiler:Executable:C}" \
        --cmake-config "CMAKE_CXX_COMPILER:STRING=%{Compiler:Executable:Cxx}" \
        --cmake-config "CMAKE_PREFIX_PATH:PATH=$QT_CMAKE_PATH" \
        --cmake-config "QT_QMAKE_EXECUTABLE:STRING=%{Qt:qmakeExecutable}" \        
        --cmake-config "CMAKE_SYSTEM_PROCESSOR:STRING=arm" \
        --cmake-config "CFG_GDBINIT_LOCATION:PATH=$workdir/.gdbinit"
}

removekit_arm() {

    #suppress errors in case e.g. a kit does not exist... at least we tried!
    SILENCE_ERRORS=1

    #toolchains
    runsdktool rmTC \
        --id "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-gcc"

    runsdktool rmTC \
        --id "ProjectExplorer.ToolChain.Gcc:$CROSS_KIT_NAME-g++"

    runsdktool rmDebugger \
        --id "$CROSS_KIT_NAME-GDB"

    runsdktool rmDev \
        --id "$CROSS_KIT_NAME-Device" \

    #Qt
    runsdktool rmQt \
        --id "$CROSS_KIT_NAME-Qt"

    runsdktool rmCMake \
        --id "$CROSS_KIT_NAME-CMake"

    runsdktool rmKit \
        --id "$CROSS_KIT_NAME-Kit"

    SILENCE_ERRORS=0
}



###########################################################################################

# check if tools are installed!
checkinstalled ninja ninja-build
checkinstalled gdb-multiarch gdb-multiarch

ide=0
cmakegui=0
terminal=0
clean=0
makecmd="ninja"

while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in

        -h|--help)
        printhelp
        exit 0
        ;;

        --clean)
        clean=1
        ;;

        --dryrun)
        dryrun=1
        ;;

        --embedded)
        embedded=1
        ;;

        --desktop)
        desktop=1
        ;;

        --ide)
        ide=1
        ;;

        --cmakegui)
        cmakegui=1
        ;;

        --terminal)
        terminal=1
        ;;

        --ninja)
        makecmd="ninja"
        ;;

        --makefiles)
        makecmd="make"
        ;;

        *)
        printhelp
        echo "Unknown option '$key'"
        exit 1
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done


if [ "$clean" == "1" ]; then
    clean_project
    exit 0
fi

mkdir -p $TMP_DIR

if [ "$ide" == "1" ]; then
    open_qtcreator
else
    create_cmake
fi


