#/bin/bash

basedir=`dirname "$(realpath $0)"`
TARGET_IP="raspberrypi.local"
TARGET_USER="pi"
TARGET_PASS="raspberry"

CMD_BASE="sshpass -p $TARGET_PASS rsync -avz -e ssh $TARGET_USER@$TARGET_IP:"

checkinstalled()
{
    if ! which $1 >/dev/null; then
        echo "$1 is not installed"
        echo "-> 'apt-get install $2'"
        exit 1
    fi
}

sync() 
{
    CMD="$CMD_BASE$1 $2"
    echo $CMD
    $CMD 
}

checkinstalled sshpass sshpass

cd $basedir

mkdir -p usr
mkdir -p opt

sync /lib .
sync /usr/include usr
sync /usr/lib usr
sync /opt/vc opt
