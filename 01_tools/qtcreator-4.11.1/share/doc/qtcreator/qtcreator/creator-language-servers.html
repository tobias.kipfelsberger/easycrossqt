<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- creator-language-server.qdoc -->
  <title>Using Language Servers | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Using Language Servers</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="creator-editor-fakevim.html" />
  <link rel="next" href="creator-mime-types.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="creator-editor-fakevim.html">Using FakeVim Mode</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-mime-types.html">Editing MIME Types</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#adding-mime-types-for-language-servers">Adding MIME Types for Language Servers</a></li>
<li class="level1"><a href="#specifying-settings-for-language-clients">Specifying Settings for Language Clients</a></li>
<li class="level1"><a href="#supported-locator-filters">Supported Locator Filters</a></li>
<li class="level1"><a href="#reporting-issues">Reporting Issues</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Using Language Servers</h1>
<span class="subtitle"></span>
<!-- $$$creator-language-servers.html-description -->
<div class="descr"> <a name="details"></a>
<p>For several programming languages, a <i>language server</i> is available that provides information about the code to IDEs as long as they support communication via the language server protocol (LSP). This enables the IDE to provide the following services:</p>
<ul>
<li><a href="creator-completing-code.html">Code completion</a></li>
<li>Highlighting the symbol under cursor</li>
<li><a href="creator-highlighting.html">Semantic highlighting</a>, as defined in <a href="https://github.com/microsoft/vscode-languageserver-node/pull/367">Proposal of the semantic highlighting protocol extension</a></li>
<li>Navigating in the code by using the <a href="creator-editor-locator.html">locator</a> or <a href="creator-coding-navigating.html#moving-to-symbol-definition-or-declaration">moving to the symbol definition</a></li>
<li>Inspecting code by viewing the document <a href="creator-sidebar-views.html#viewing-defined-types-and-symbols">outline</a></li>
<li><a href="creator-editor-refactoring.html#finding-symbols">Finding references to symbols</a></li>
<li>Code actions</li>
<li>Integrating diagnostics from the language server</li>
</ul>
<p>By providing a client for the language server protocol, Qt Creator can support the above features for several other programming languages besides C++. However, the client does not support language servers that require special handling.</p>
<a name="adding-mime-types-for-language-servers"></a>
<h2 id="adding-mime-types-for-language-servers">Adding MIME Types for Language Servers</h2>
<p>Qt Creator uses the <a href="https://www.iana.org/assignments/media-types/media-types.xhtml">MIME type</a> of the file to determine which language server to request information from when you open a file for editing. Add new MIME types or file patterns to match language servers. If you do not set at least one MIME type or file pattern, no files will be sent to the language server. This is done to avoid unnecessary traffic and inaccurate information, as files are only sent to the languge server if they are known to be handled by it. For more information about how Qt Creator uses MIME types, see <a href="creator-mime-types.html">Editing MIME Types</a>.</p>
<a name="specifying-settings-for-language-clients"></a>
<h2 id="specifying-settings-for-language-clients">Specifying Settings for Language Clients</h2>
<p>To use a language server:</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Language Client</b> (or <b>Qt Creator</b> &gt; <b>Preferences</b> &gt; <b>Language Client</b> &gt; on <a href="nolink">macOS</a>) to view a list of language servers.<p class="centerAlign"><img src="images/qtcreator-language-client-options.png" alt="&quot;Language client options page&quot;" /></p></li>
<li>Select the check box next to the language server name to enable the language server.</li>
<li>Select <b>Add</b> to add language servers.</li>
<li>In the <b>Name</b> field, enter a name for the language server. Select the <img src="images/replace.png" alt="" /> (<b>Variables</b>) button to use a variable for the server name. For more information, see <a href="creator-build-settings.html#using-qt-creator-variables">Using Qt Creator Variables</a>.</li>
<li>In the <b>Language</b> field, select <b>Set MIME Types</b> to select the MIME types of the files to send to the language server. In the field below, you can enter file patterns to extend the MIME types, separated by semicolons.</li>
<li>In the <b>Startup behavior</b> field, select whether the language server is started when Qt Creator starts or when a project or file with a matching MIME type is opened. The <b>General Messages</b> <a href="creator-output-panes.html">output pane</a> displays information about the connection to the language server.</li>
<li>In the <b>Capabilities</b> field, you can see the features that are supported by the language server. Only some of them are implemented by Qt Creator.</li>
<li>In the <b>Executable</b> field, enter the path to the language server executable.</li>
<li>In the <b>Arguments</b> field, enter any required command line arguments. Select <b>Variables</b> to use variables as arguments.</li>
</ol>
<p>To remove language servers from the list, select <b>Delete</b>.</p>
<a name="supported-locator-filters"></a>
<h2 id="supported-locator-filters">Supported Locator Filters</h2>
<p>The locator enables you to browse not only files, but any items defined by <i>locator filters</i>. The language client plugin supports the following locator filters:</p>
<ul>
<li>Locating symbols in the current project (<code>:</code>)</li>
<li>Locating symbols in the current document (<code>.</code>)</li>
<li>Locating class (<code>c</code>), enum, and function (<code>m</code>) definitions in your project</li>
</ul>
<a name="reporting-issues"></a>
<h2 id="reporting-issues">Reporting Issues</h2>
<p>The language service client has been mostly tested with Python. If problems arise when you try it or some other language, please select <b>Help</b> &gt; <b>Report Bug</b> to report them in the Qt Bug Tracker. The reports should include Qt Creator console output with the environment variable <code>QT_LOGGING_RULES=qtc.languageclient.*=true</code> set.</p>
</div>
<!-- @@@creator-language-servers.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="creator-editor-fakevim.html">Using FakeVim Mode</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-mime-types.html">Editing MIME Types</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
