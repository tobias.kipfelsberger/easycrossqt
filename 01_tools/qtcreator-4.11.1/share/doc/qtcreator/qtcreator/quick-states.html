<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- qtquick-states.qdoc -->
  <title>Adding States | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Adding States</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="quick-connections-backend.html" />
  <link rel="next" href="qmldesigner-pathview-editor.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="quick-connections-backend.html">Managing C++ Backend Objects</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="qmldesigner-pathview-editor.html">Editing PathView Properties</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#using-states">Using States</a></li>
<li class="level2"><a href="#using-scxml-state-machines">Using SCXML State Machines</a></li>
<li class="level1"><a href="#animating-transitions-between-states">Animating Transitions Between States</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Adding States</h1>
<span class="subtitle"></span>
<!-- $$$quick-states.html-description -->
<div class="descr"> <a name="details"></a>
<p>User interfaces are designed to present different interface configurations in different scenarios, or to modify their appearances in response to user interaction. Often, there are a set of changes that are made concurrently, such that the interface could be seen to be internally changing from one <i>state</i> to another.</p>
<p>This applies generally to interfaces regardless of their complexity. A photo viewer may initially present images in a grid, and when an image is clicked, change to a detailed state where the individual image is expanded and the interface is changed to present new options for image editing. At the other end of the scale, when a button is pressed, it may change to a <i>pressed</i> state in which its color and position are modified so that it appears to be pressed down.</p>
<p>In QML, any item can change between different states to apply sets of changes that modify the properties of relevant items. Each state can present a different configuration that can, for example:</p>
<ul>
<li>Show some UI items and hide others.</li>
<li>Present different available actions to the user.</li>
<li>Start, stop or pause animations.</li>
<li>Execute some script required in the new state.</li>
<li>Change a property value for a particular item.</li>
<li>Show a different view.</li>
</ul>
<p>The <b>States</b> pane displays the different <a href="http://doc.qt.io/qt-5/qml-qtqml-statemachine-state.html">states</a> of the component in the Design mode. The <b>States</b> pane is collapsed by default to save space. Select <b>Expand</b> in the context menu to view the whole pane.</p>
<p class="centerAlign"><img src="images/qmldesigner-transitions.png" alt="&quot;States pane&quot;" /></p><p>To add states, click the <img src="images/plus.png" alt="" /> button. Then modify the new state in the editor. For example, to change the appearance of a button, you can hide the button image and show another image in its place. Or, to add movement to the view, you can change the position of an object on the canvas and then add animation to the change between the states.</p>
<p>To determine when the state should be applied, select <b>Set when Condition</b> in the menu and specify a <a href="http://doc.qt.io/qt-5/qml-qtquick-state.html#when-prop">when</a> property for the state. Set the value of the property to an expression that evaluates to <code>true</code> when you want the state to be applied. In the binding editor, select the component and property to create the expression. For example, to change the state when a button is pressed, you could select a button component and its pressed property.</p>
<p class="centerAlign"><img src="images/qtquick-states-binding-editor.png" alt="&quot;Binding editor in States pane&quot;" /></p><p>You can preview the states in the <b>States</b> pane and click them to switch between states on the canvas.</p>
<a name="using-states"></a>
<h2 id="using-states">Using States</h2>
<p>QML states typically describe user interface configurations, such as the UI controls, their properties and behavior and the available actions. For example, you can use states to create two views.</p>
<p>To add states, click the empty slot in the <b>States</b> pane. Then modify the new state in the <b>Form Editor</b> or the <b>Properties</b> pane.</p>
<p class="centerAlign"><img src="images/qmldesigner-states.png" alt="&quot;States pane&quot;" /></p><p>The properties that you change in a state are highlighted with blue color. In the <b>Text Editor</b>, you can see the changes recorded as changes to the base state.</p>
<p>To keep the QML code clean, you should create a base state that contains all the types you will need in the application. You can then create states, in which you hide and show a set of items and modify their properties. This allows you to:</p>
<ul>
<li>Align items on different views with each other.</li>
<li>Avoid excessive property changes. If an item is invisible in the base state, you must define all changes to its child types as property changes, which leads to complicated QML code.</li>
<li>Minimize the differences between the base state and the other states to keep the QML code short and readable and to improve performance.</li>
<li>Avoid problems when using transitions and animation when changing states.</li>
</ul>
<p>To create views for an application by using states:</p>
<ol class="1" type="1"><li>In the base state, add all items you will need in the application (1). While you work on one view, you can click the <img src="images/eye_open.png" alt="" /> icon to hide items on the canvas that are not part of a view.</li>
<li>In the <b>States</b> pane, click the empty slot to create a new state and give it a name. For example, <code>Normal</code>.</li>
<li>In the <b>Properties</b> pane (2), deselect the <b>Visibility</b> check box or set <b>Opacity</b> to 0 for each item that is not needed in this view. If you specify the setting for the parent item, all child items inherit it and are also hidden.<p class="centerAlign"><img src="images/qmldesigner-screen-design.png" alt="&quot;Designing views&quot;" /></p></li>
<li>Create additional states for each view and set the visibility or opacity of the items in the view.</li>
<li>To determine which view opens when the application starts, use the <b>Text Editor</b> to set the state of the root item of the .qml file, as specified by the following code snippet:<pre class="qml">

  <span class="type"><a href="http://doc.qt.io/qt-5/qml-qtquick-item.html">Item</a></span> {
      <span class="name">state</span>: <span class="string">&quot;Normal&quot;</span>
  }

</pre>
</li>
</ol>
<a name="using-scxml-state-machines"></a>
<h3 id="using-scxml-state-machines">Using SCXML State Machines</h3>
<p>To use QML together with an SCXML state machine, add states and bind them to the state machine in the <b>Backends</b> tab in the Design mode, as described in <a href="quick-connections-backend.html">Managing C++ Backend Objects</a>.</p>
<p>In the <b>States</b> pane, you can edit the <code>when</code> condition of states to map QML states to the states of the SCXML state machine. For an example, see <a href="http://doc.qt.io/qt-5/qtscxml-trafficlight-qml-dynamic-example.html">Qt SCXML Traffic Light QML Example (Dynamic)</a>.</p>
<p class="centerAlign"><img src="images/qmldesigner-states-when-condition.png" alt="" /></p><p>If you add animation to the states, you can run the application to test the animation.</p>
<a name="animating-transitions-between-states"></a>
<h2 id="animating-transitions-between-states">Animating Transitions Between States</h2>
<p>To make movement between states smooth, you can specify transitions. You can use different types of animated transitions. For example, you can animate changes to property values and colors. You can use rotation animation to control the direction of rotation. For more information, see <a href="http://doc.qt.io/qt-5/qtquick-statesanimations-animations.html">Animation and Transitions in Qt Quick</a>.</p>
<p>You can use the <code>ParallelAnimation</code> type to start several animations at the same time. Or use the <code>SequentialAnimation</code> type to run them one after another.</p>
<p>You can use the <b>Text Editor</b> to specify transitions. For more information, see <a href="http://doc.qt.io/qt-5/qmlexampletoggleswitch.html#transition">Transition</a>.</p>
<p>Alternatively, you can use the <b>Timeline</b> view to animate the properties of UI components and to bind the animations to states. For more information, see <a href="studio-timeline.html#binding-animations-to-states">Binding Animations to States</a>.</p>
</div>
<!-- @@@quick-states.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="quick-connections-backend.html">Managing C++ Backend Objects</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="qmldesigner-pathview-editor.html">Editing PathView Properties</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
