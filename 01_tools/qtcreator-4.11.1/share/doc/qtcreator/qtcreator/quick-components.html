<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- qtquick-components.qdoc -->
  <title>Creating Components | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Creating Components</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="creator-using-qt-quick-designer.html" />
  <link rel="next" href="quick-buttons.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="creator-using-qt-quick-designer.html">Editing QML Files in Design Mode</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="quick-buttons.html">Creating Buttons</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#adding-components-to-designs">Adding Components to Designs</a></li>
<li class="level1"><a href="#using-basic-qml-types">Using Basic QML Types</a></li>
<li class="level1"><a href="#using-data-models">Using Data Models</a></li>
<li class="level1"><a href="#positioning-items-in-uis">Positioning Items in UIs</a></li>
<li class="level2"><a href="#setting-bindings">Setting Bindings</a></li>
<li class="level2"><a href="#setting-anchors-and-margins">Setting Anchors and Margins</a></li>
<li class="level2"><a href="#aligning-and-distributing-items">Aligning and Distributing Items</a></li>
<li class="level2"><a href="#using-positioners">Using Positioners</a></li>
<li class="level2"><a href="#using-layouts">Using Layouts</a></li>
<li class="level2"><a href="#editing-stack-layouts">Editing Stack Layouts</a></li>
<li class="level2"><a href="#organizing-items">Organizing Items</a></li>
<li class="level1"><a href="#adding-user-interaction-methods">Adding User Interaction Methods</a></li>
<li class="level1"><a href="#history-of-qt-quick-controls">History of Qt Quick Controls</a></li>
<li class="level1"><a href="#creating-components-in-design-mode">Creating Components in Design Mode</a></li>
<li class="level1"><a href="#moving-within-components">Moving Within Components</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Creating Components</h1>
<span class="subtitle"></span>
<!-- $$$quick-components.html-description -->
<div class="descr"> <a name="details"></a>
<p>A <a href="creator-glossary.html#glossary-component">component</a> provides a way of defining a new visual item that you can re-use in other QML files. A component is like a black box; it interacts with the outside world through properties, signals, and slots, and is generally defined in its own QML file. You can import components to applications.</p>
<p>The <b>Library</b> pane lists the available QML types, UI components, resources, and imports.</p>
<p class="centerAlign"><img src="images/qmldesigner-qml-components.png" alt="&quot;QML Components&quot;" /></p><p>The <b>QML Types</b> tab displays the QML types grouped by category: your own QML components, basic types, layouts, positioner types, and views.</p>
<p>Sets of UI components with the look and feel of a particular mobile device platform have been defined for Qt Quick 1. Since Qt 5.1, ready-made Qt Quick Controls, Dialogs, and Layouts are available for creating user interfaces using Qt Quick 2. The components and controls are based on standard QML types. To view the components and controls in the <b>Library</b>, import the component sets in <b>Imports</b>.</p>
<p>The <b>Qt Quick Application</b> wizards for a particular platform add the import statements automatically. You can remove import statements in <b>Imports</b></p>
<p><b>Resources</b> displays the images and other files that you copy to the project folder (to the same subfolder as the QML files).</p>
<a name="adding-components-to-designs"></a>
<h2 id="adding-components-to-designs">Adding Components to Designs</h2>
<p class="centerAlign"><img src="images/qmldesigner-visual-editor.png" alt="&quot;Design mode&quot;" /></p><ol class="1" type="1"><li>Drag and drop components from the <b>Library</b> (2) to the <b>Navigator</b> (3) or <b>Form Editor</b> (1).</li>
<li>Select components in the <b>Navigator</b> to edit their properties in the <b>Properties</b> pane (4). For more information, see <a href="qtquick-properties.html">Specifying Item Properties</a>.</li>
<li>Connect components to signals or create bindings between components in the <b>Connections</b> pane (5). For more information, see <a href="qmldesigner-connections.html">Adding Connections</a>.</li>
<li>Add states to apply sets of changes to the properties of one or several components in the <b>States</b> pane (6). For more information, see <a href="quick-states.html">Adding States</a>.</li>
</ol>
<a name="using-basic-qml-types"></a>
<h2 id="using-basic-qml-types">Using Basic QML Types</h2>
<p>You can use the following QML types to create components:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-animatedimage.html">Animated Image</a> provides a way to play animations stored as images containing a series of frames, such as those stored in GIF files.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-borderimage.html">Border Image</a> uses an image as a border or background.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-image.html">Image</a> adds a bitmap to the scene. You can stretch and tile images.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-item.html">Item</a> is the most basic of all visual types in QML. Even though it has no visual appearance, it defines all the properties that are common across visual types, such as the x and y position, width and height, anchoring, and key handling.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-rectangle.html">Rectangle</a> adds a rectangle that is painted with a solid fill color and an optional border. You can use the radius property to create rounded rectangles.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-text.html">Text</a> adds formatted read-only text.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-textedit.html">Text Edit</a> adds a single line of editable formatted text that can be validated.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-textinput.html">Text Input</a> adds a single line of editable plain text that can be validated.</li>
</ul>
<a name="using-data-models"></a>
<h2 id="using-data-models">Using Data Models</h2>
<p>You can create the following types of views to organize items provided by <a href="http://doc.qt.io/qt-5/qtquick-modelviewsdata-modelview.html">data models</a>:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-gridview.html">Grid View</a> provides a grid vizualization of a model.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-listview.html">List View</a> provides a list vizualization of a model.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-pathview.html">Path View</a> visualizes the contents of a model along a path. For more information, see <a href="qmldesigner-pathview-editor.html">Editing PathView Properties</a>.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-scrollview.html">Scroll View</a> provides scrolling for user-defined content. It can be used instead of a <a href="http://doc.qt.io/qt-5/qml-qtquick-flickable.html">Flickable</a> item.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-stackview.html">Stack View</a> provides a stack-based navigation model.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-swipeview.html">Swipe View</a> enables users to navigate pages by swiping sideways.</li>
</ul>
<p>When you add a <a href="http://doc.qt.io/qt-5/qml-qtquick-gridview.html">Grid View</a>, <a href="http://doc.qt.io/qt-5/qml-qtquick-listview.html">List View</a>, or <a href="http://doc.qt.io/qt-5/qml-qtquick-pathview.html">Path View</a>, the ListModel and the delegate component that creates an instance for each item in the model are added automatically. You can edit item properties in the <b>Properties</b> pane or in the <b>Text Editor</b>. You can also replace the default model and delegate with other, more complex models and delegates in the <b>Text Editor</b>. <a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-itemdelegate.html">Item Delegate</a> and <a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-swipedelegate.html">Swipe Delegate</a> delegate components are also available in the <b>Library</b>.</p>
<a name="positioning-items-in-uis"></a>
<h2 id="positioning-items-in-uis">Positioning Items in UIs</h2>
<p>The position of an item in the UI can be either absolute or relative to other items. If you are designing a static UI, <a href="http://doc.qt.io/qt-5/qtquick-positioning-topic.html#manual-positioning">manual positioning</a> provides the most efficient form of positioning items. For a dynamic UI, you can employ the following positioning methods provided by Qt Quick:</p>
<ul>
<li><a href="quick-components.html#setting-bindings">Setting Bindings</a></li>
<li><a href="quick-components.html#setting-anchors-and-margins">Setting Anchors and Margins</a></li>
<li><a href="quick-components.html#aligning-and-distributing-items">Aligning and Distributing Items</a></li>
<li><a href="quick-components.html#using-positioners">Using Positioners</a></li>
<li><a href="quick-components.html#using-layouts">Using Layouts</a></li>
<li><a href="quick-components.html#organizing-items">Organizing Items</a></li>
</ul>
<a name="setting-bindings"></a>
<h3 id="setting-bindings">Setting Bindings</h3>
<p><a href="http://doc.qt.io/qt-5/qtquick-positioning-topic.html#positioning-with-bindings">Property binding</a> is a declarative way of specifying the value of a property. Binding allows a property value to be expressed as a JavaScript expression that defines the value relative to other property values or data accessible in the application. The property value is automatically kept up to date if the other properties or data values change.</p>
<p>Property bindings are created implicitly in QML whenever a property is assigned a JavaScript expression. To set JavaScript expressions as values of properties in the Design mode, select the <img src="images/action-icon.png" alt="" /> (<b>Actions</b>) menu next to a property, and then select <b>Set Binding</b>.</p>
<p class="centerAlign"><img src="images/qmldesigner-set-expression.png" alt="&quot;Type properties context menu&quot;" /></p><p>In the <b>Binding Editor</b>, select an item and a property from lists of available items and their properties.</p>
<p class="centerAlign"><img src="images/qmldesigner-binding-editor.png" alt="&quot;Binding Editor&quot;" /></p><p>Alternatively, start typing a string and press <b>Ctrl+Space</b> to display a list of properties, IDs, and code snippets. When you enter a period (.) after a property name, a list of available values is displayed. Press <b>Enter</b> to accept the first suggestion in the list and to complete the code.</p>
<p>When a binding is set, the <b>Actions</b> menu icon changes to <img src="images/action-icon-binding.png" alt="" /> . To remove bindings, select <b>Reset</b> in the <b>Actions</b> menu.</p>
<p>You can set bindings also in the <b>Connections</b> view. For more information, see <a href="quick-property-bindings.html">Adding Bindings Between Properties</a>.</p>
<p>For more information on the JavaScript environment provided by QML, see <a href="http://doc.qt.io/qt-5/qtqml-javascript-topic.html">Integrating QML and JavaScript</a>.</p>
<p>Bindings are a black box for the Design mode and using them might have a negative impact on performance, so consider setting anchors and margins for items, instead. For example, instead of setting <code>parent.width</code> for an item, you could anchor the item to its sibling items on the left and the right.</p>
<a name="setting-anchors-and-margins"></a>
<h3 id="setting-anchors-and-margins">Setting Anchors and Margins</h3>
<p>In an <a href="http://doc.qt.io/qt-5/qtquick-positioning-topic.html#anchors">anchor-based</a> layout, each QML type can be thought of as having a set of invisible <i>anchor</i> lines: top, bottom, left, right, fill, horizontal center, vertical center, and baseline.</p>
<p>In the <b>Layout</b> pane you can set anchors and margins for items. To set the anchors of an item, click the anchor buttons. You can combine the top/bottom, left/right, and horizontal/vertical anchors to anchor items in the corners of the parent item or center them horizontally or vertically within the parent item.</p>
<p class="centerAlign"><img src="images/qmldesigner-anchor-buttons.png" alt="&quot;Anchor buttons&quot;" /></p><p>For convenience, you can click the <img src="images/anchor-fill.png" alt="" /> (<b>Fill to Parent</b>) toolbar button to apply fill anchors to an item and the <img src="images/qtcreator-anchors-reset-icon.png" alt="" /> (<b>Reset Anchors</b>) button to reset the anchors to their saved state.</p>
<p>You can specify the baseline anchor in the <b>Text Editor</b> in the Design mode.</p>
<p>For performance reasons, you can only anchor an item to its siblings and direct parent. By default, an item is anchored to its parent when you use the anchor buttons. Select a sibling of the item in the <b>Target</b> field to anchor to it, instead.</p>
<p>Arbitrary anchoring is not supported. For example, you cannot specify: <code>anchor.left: parent.right</code>. You have to specify: <code>anchor.left: parent.left</code>. When you use the anchor buttons, anchors to the parent item are always specified to the same side. However, anchors to sibling items are specified to the opposite side: <code>anchor.left: sibling.right</code>. This allows you to keep sibling items together.</p>
<p>In the following image, <b>Rectangle 2</b> is anchored to <b>Rectangle 1</b> on its left and to the bottom of its parent.</p>
<p class="centerAlign"><img src="images/qmldesigner-anchors.png" alt="&quot;Anchoring sibling items&quot;" /></p><p>The anchors for <b>Rectangle 2</b> are specified as follows in code:</p>
<pre class="qml">

  <span class="type"><a href="http://doc.qt.io/qt-5/qml-qtquick-rectangle.html">Rectangle</a></span> {
      <span class="name">id</span>: <span class="name">rectangle2</span>
      <span class="name">anchors</span>.left: <span class="name">rectangle1</span>.<span class="name">right</span>
      <span class="name">anchors</span>.leftMargin: <span class="number">10</span>
      <span class="name">anchors</span>.bottom: <span class="name">parent</span>.<span class="name">bottom</span>
      <span class="name">anchors</span>.bottomMargin: <span class="number">10</span>
      //
  }

</pre>
<p>Margins specify the amount of empty space to leave to the outside of an item. Margins only have meaning for anchors. They do not take any effect when using layouts or absolute positioning.</p>
<a name="aligning-and-distributing-items"></a>
<h3 id="aligning-and-distributing-items">Aligning and Distributing Items</h3>
<p>When you're working with a group of items, you can select them to align and distribute them evenly. As the positions of the items are fixed, you cannot apply these functions to anchored items. For scalability, you can anchor the aligned and distributed items when your design is ready.</p>
<p class="centerAlign"><img src="images/qmldesigner-alignment.png" alt="&quot;Aligning sibling items&quot;" /></p><p>Select the buttons in the <b>Align</b> group to align the top/bottom or left/right edges of the items in the group to the one farthest away from the center of the group. For example, when left-aligning, the items are aligned to the leftmost item. You can also align the horizontal/vertical centers of items, or both, as in the image above.</p>
<p>In the <b>Align to</b> field, select whether to align the items in respect to the selection, the root item, or a <i>key object</i> that you select in the <b>Key object</b> field. The key object must be a part of the selection.</p>
<p>You can distribute either <i>objects</i> or the <i>spacing</i> between them. If the objects or spacing cannot be distributed to equal pixel values without ending up with half pixels, you receive a notification. You can either allow Qt Design Studio to distribute objects or spacing using the closest values possible or tweak your design so that the objects and spacing can be distributed perfectly.</p>
<p>When distributing objects, you can select whether the distance between them is calculated from their top/bottom or left/right edges or their horizontal/vertical center.</p>
<p class="centerAlign"><img src="images/qmldesigner-distribute-objects.png" alt="&quot;Distribute objects buttons&quot;" /></p><p>You can distribute spacing either evenly within a target area or at specified distances, calculated from a starting point.</p>
<p>You can select the orientation in which the objects are distributed evenly within the target area: horizontally along the x axis or vertically along the y axis.</p>
<p class="centerAlign"><img src="images/qmldesigner-distribute-spacing-evenly.png" alt="&quot;Distribute spacing evenly&quot;" /></p><p>Alternatively, you can distribute spacing in pixels by selecting one of the starting point buttons: left/right or top/bottom edge of the target area, or its horizontal/vertical center. Note that some items might end up outside the target area.</p>
<p class="centerAlign"><img src="images/qmldesigner-distribute-spacing-pixels.png" alt="&quot;Distribute spacing in pixels&quot;" /></p><p>You can set the space between objects in pixels. You can disable the distribution of spacing in pixels by clicking the <img src="images/qmldesigner-distribute-spacing-x.png" alt="" /> button.</p>
<a name="using-positioners"></a>
<h3 id="using-positioners">Using Positioners</h3>
<p><a href="http://doc.qt.io/qt-5/qtquick-positioning-topic.html#positioners">Positioner items</a> are container items that manage the positions of items in a declarative user interface. Positioners behave in a similar way to the layout managers used with standard Qt widgets, except that they are also containers in their own right.</p>
<p>You can use the following positioners to arrange items in UIs:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-column.html">Column</a> arranges its child items vertically.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-row.html">Row</a> arranges its child items horizontally.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-grid.html">Grid</a> arranges its child items so that they are aligned in a grid and are not overlapping.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-flow.html">Flow</a> arranges its child items side by side, wrapping as necessary.</li>
</ul>
<p>To position several items in a <b>Column</b>, <b>Row</b>, <b>Grid</b>, or <b>Flow</b>, select the items on the canvas, and then select <b>Position</b> in the context menu.</p>
<a name="using-layouts"></a>
<h3 id="using-layouts">Using Layouts</h3>
<p>Since Qt 5.1, you can use QML types in the <a href="http://doc.qt.io/qt-5/qtquicklayouts-index.html">Qt Quick Layouts</a> module to arrange Qt Quick items in UIs. Unlike positioners, they manage both the positions and sizes of items in a declarative interface. They are well suited for resizable UIs.</p>
<p>You can use the following layout types to arrange items in UIs:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-layouts-layout.html">Layout</a> provides attached properties for items pushed onto a column, row, or grid layout.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-layouts-columnlayout.html">Column Layout</a> provides a grid layout with only one column.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-layouts-rowlayout.html">Row Layout</a> provides a grid layout with only one row.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-layouts-gridlayout.html">Grid Layout</a> provides a way of dynamically arranging items in a grid.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-layouts-stacklayout.html">Stack Layout</a> provides a stack of items where only one item is visible at a time.</li>
</ul>
<p>To lay out several items in a column, row, grid, or <b>Stack Layout</b>, select the items in the <b>Form Editor</b>, and then select <b>Layout</b> in the context menu.</p>
<p>You can also click the <img src="images/column.png" alt="" /> (<b>Column Layout</b>), <img src="images/row.png" alt="" /> (<b>Row Layout</b>), and <img src="images/grid.png" alt="" /> (<b>Grid Layout</b>) toolbar buttons to apply layouts to the selected items.</p>
<p>To make an item within a layout as wide as possible while respecting the given constraints, select the item on the canvas and then select <b>Layout</b> &gt; <b>Fill Width</b> in the context menu. To make the item as high as possible, select <b>Fill Height</b>.</p>
<a name="editing-stack-layouts"></a>
<h3 id="editing-stack-layouts">Editing Stack Layouts</h3>
<p class="centerAlign"><img src="images/qtquick-designer-stacked-view.png" alt="" /></p><p>To add items to a <b>Stack Layout</b>, select the <img src="images/plus.png" alt="" /> button next to the type name in the <b>Form Editor</b>. To move between items, select the <img src="images/prev.png" alt="" /> (<b>Previous</b>) and <img src="images/next.png" alt="" /> (<b>Next</b>) buttons.</p>
<p>To add a tab bar to a stack layout, select <b>Stacked Container</b> &gt; <b>Add Tab Bar</b>.</p>
<p>To raise or lower the stacking order of an item, select <b>Stacked Container</b> &gt; <b>Increase Index</b> or <b>Decrease Index</b>.</p>
<a name="organizing-items"></a>
<h3 id="organizing-items">Organizing Items</h3>
<p>Since Qt 5.7, you can use the following <a href="http://doc.qt.io/qt-5/qtquickcontrols-index.html">Qt Quick Controls</a> types to organize items in UIs:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-frame.html">Frame</a> places a logical group of controls within a visual frame.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-groupbox.html">Group Box</a> is used to lay out a logical group of controls together, within a titled visual frame.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-label.html">Label</a> is a text label with inherited styling and font.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-page.html">Page</a> provides a styled page control with support for a header and footer.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-pageindicator.html">Page Indicator</a> indicates the currently active page.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-pane.html">Pane</a> provides a background matching with the application style and theme.</li>
</ul>
<a name="adding-user-interaction-methods"></a>
<h2 id="adding-user-interaction-methods">Adding User Interaction Methods</h2>
<p>You can use the following QML types to add basic interaction methods to UIs:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-flickable.html">Flickable</a> items can be flicked horizontally or vertically.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-focusscope.html">Focus Scope</a> assists in keyboard focus handling when building reusable QML components.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-mousearea.html">Mouse Area</a> enables simple mouse handling.</li>
</ul>
<p>Since Qt 5.7, you can also use the following <a href="http://doc.qt.io/qt-5/qtquickcontrols-index.html">Qt Quick Controls</a> types to inform users about the progress of the application or to gather input from the user:</p>
<ul>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-busyindicator.html">Busy Indicator</a> indicates activity while content is being loaded.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-button.html">Button</a> provides a push button that you can associate with an action.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-checkbox.html">Check Box</a> provides an option button that can be toggled on (checked) or off (unchecked).</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-checkdelegate.html">Check Delegate</a> presents an item delegate that can be toggled on (checked) or off (unchecked).</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-combobox.html">Combo Box</a> is a combined button and popup list that is populated by using a data model.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-delaybutton.html">Delay Button</a> provides an option button that is triggered when held down long enough.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-dial.html">Dial</a> is a circular dial that is rotated to set a value.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-progressbar.html">Progress Bar</a> indicates the progress of an operation.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-radiobutton.html">Radio Button</a> provides an option button that can be switched on (checked) or off (unchecked).</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-radiodelegate.html">Radio Delegate</a> presents an item delegate that can be toggled on (checked) or off (unchecked).</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-rangeslider.html">Range Slider</a> enables users to select a range of values by sliding two handles along a track.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-roundbutton.html">Round Button</a> provides a push button with rounded corners that you can associate with an action.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-slider.html">Slider</a> selects a value by sliding a handle along a track.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-spinbox.html">Spin Box</a> enables the user to specify a value by clicking the up or down buttons, by pressing up or down on the keyboard, or by entering a value in the box.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-switch.html">Switch</a> is an option button that can be toggled on or off.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-switchdelegate.html">Switch Delegate</a> presents an item delegate with a switch indicator that can be toggled on or off.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-tabbar.html">Tab Bar</a> enables users to switch between different views or subtasks.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-tabbutton.html">Tab Button</a> is a button that is functionally similar to <b>Button</b>, but provides a look that is more suitable for a <b>Tab Bar</b>.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-textarea.html">Text Area</a> displays multiple lines of editable formatted text.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-textfield.html">Text Field</a> displays a single line of editable plain text.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-toolbar.html">Tool Bar</a> is a container of application-wide and context sensitive actions and controls, such as navigation buttons and search fields.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-toolbutton.html">Tool Button</a> is a button that is functionally similar to <b>Button</b>, but provides a look that is more suitable for a <b>Tool Bar</b>.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-toolseparator.html">Tool Separator</a> separates a group of items from adjacent items on a <b>Tool Bar</b>.</li>
<li><a href="http://doc.qt.io/qt-5/qml-qtquick-controls2-tumbler.html">Tumbler</a> is a spinnable wheel of items that can be selected.</li>
</ul>
<p>You can also use the <a href="https://doc-snapshots.qt.io/qt5-5.13/qml-qtquick-dialogs-dialog.html">Dialog</a> type in the Qt Quick Dialogs module to wrap arbitrary content into a dialog window including a row of platform-tailored buttons.</p>
<a name="history-of-qt-quick-controls"></a>
<h2 id="history-of-qt-quick-controls">History of Qt Quick Controls</h2>
<p>In Qt 4, ready-made Qt Quick 1 Components were provided for creating UIs with a native look and feel for a particular target platform. In Qt 5.1, Qt Quick Controls, Dialogs, and Layouts were added for creating classic desktop-style user interfaces using Qt Quick 2.1&#x2e; The Qt Quick Controls Styles could be used to customize Qt Quick Controls.</p>
<p>Since Qt 5.7, <a href="http://doc.qt.io/qt-5/qtquickcontrols-index.html">Qt Quick Controls 2</a> replace Qt Quick Controls 1 and Qt Labs Controls. They provide lightweight QML types for creating performant user interfaces for <a href="creator-glossary.html#glossary-device">devices</a>.</p>
<p>Qt Quick Controls 2 achieve improved efficiency by employing a simplified <a href="http://doc.qt.io/qt-5/qtquickcontrols2-styles.html">styling architecture</a> when compared to Qt Quick Controls, on which the module is based. The visual editor reads the <code>qtquickcontrols2.conf</code> file that specifies the preferred style and some style-specific arguments. To change the style, select another style from the list on the toolbar. This enables you to check how your UI looks when using the available styles.</p>
<p>For an example of defining your own style and using it in the Design mode, see <a href="http://doc.qt.io/qt-5/qtquickcontrols-flatstyle-example.html">Qt Quick Controls 2 - Flat Style</a>.</p>
<p>For more information about how to customize a particular control, see <a href="http://doc.qt.io/qt-5/qtquickcontrols2-customize.html#customization-reference">Customization Reference</a>.</p>
<p>Qt Quick Controls 2 work in conjunction with Qt Quick and Qt Quick Layouts.</p>
<p>The Qt Creator project wizards create Qt Quick applications that use Qt Quick 2 types or Qt Quick Controls 2 types.</p>
<p>Even if you use Qt Quick Controls 2, you can still write cross-platform applications, by using different sets of QML files for each platform.</p>
<p>Some ready-made controls, such as a gauge, dial, status indicator, and tumbler, are provided by the <a href="http://doc.qt.io/qt-5/qtquickextras-index.html">Qt Quick Extras</a> module.</p>
<a name="creating-components-in-design-mode"></a>
<h2 id="creating-components-in-design-mode">Creating Components in Design Mode</h2>
<ol class="1" type="1"><li>Select <b>File</b> &gt; <b>New File or Project</b> &gt; <b>Qt</b> &gt; <b>QML File (Qt Quick 2)</b> &gt; <b>Choose</b> to create a new .qml file.<p><b>Note: </b>Components are listed in the <b>QML Components</b> section of the <b>Library</b> only if the filename begins with a capital letter.</p></li>
<li>Click <b>Design</b> to open the .qml file in the Design mode.</li>
<li>Drag and drop a QML type from the <b>Library</b> to the <b>Navigator</b> or <b>Form Editor</b>.</li>
<li>Edit its properties in the <b>Properties</b> pane.<p>The available properties depend on the QML type.</p>
</li>
</ol>
<p>The following sections contain examples of how to create some common components:</p>
<ul>
<li><a href="quick-buttons.html">Creating Buttons</a></li>
<li><a href="quick-scalable-image.html">Creating Scalable Buttons and Borders</a></li>
</ul>
<a name="moving-within-components"></a>
<h2 id="moving-within-components">Moving Within Components</h2>
<p>Components can consist of several other components. To view the component hierarchy as a bread crumb path when you edit a component on the canvas, select <b>Go into Component</b> or press <b>F2</b>. Click the component names in the path to navigate to them. You can easily navigate back to the top level when you are done editing the component.</p>
<p class="centerAlign"><img src="images/qmldesigner-breadcrumbs.png" alt="&quot;Go into Component command&quot;" /></p></div>
<!-- @@@quick-components.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="creator-using-qt-quick-designer.html">Editing QML Files in Design Mode</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="quick-buttons.html">Creating Buttons</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
