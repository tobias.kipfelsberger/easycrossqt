<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- creator-projects-targets.qdoc -->
  <title>Adding Kits | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Adding Kits</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="creator-configuring-projects.html" />
  <link rel="next" href="creator-project-qmake.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="creator-configuring-projects.html">Configuring Projects</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-project-qmake.html">Adding Qt Versions</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#filtering-kit-settings">Filtering Kit Settings</a></li>
<li class="level1"><a href="#specifying-kit-settings">Specifying Kit Settings</a></li>
<li class="level2"><a href="#using-ninja-as-a-cmake-generator">Using Ninja as a CMake Generator</a></li>
<li class="level1"><a href="#editing-qbs-profiles">Editing Qbs Profiles</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Adding Kits</h1>
<span class="subtitle"></span>
<!-- $$$creator-targets.html-description -->
<div class="descr"> <a name="details"></a>
<p>Qt Creator groups settings used for building and running projects as kits to make cross-platform and cross-configuration development easier. Each kit consists of a set of values that define one environment, such as a <a href="creator-glossary.html#glossary-device">device</a>, compiler, Qt version, and debugger command to use, and some metadata, such as an icon and a name for the kit. Once you have defined kits, you can select them to build and run projects.</p>
<p>Qt Creator supports development for the desktop and for the following types of devices:</p>
<ul>
<li>Android Device</li>
<li>Bare Metal Device</li>
<li><a href="http://doc.qt.io/QtForDeviceCreation/qtb2-index.html">Boot2Qt</a> Device (commercial only)</li>
<li><a href="http://doc.qt.io/emulator/index.html">Boot2Qt Emulator Device</a> (commercial only)</li>
<li>Generic Linux Device</li>
<li>iOS Device</li>
<li>iOS Simulator</li>
<li>QNX Device</li>
<li>Windows Phone</li>
<li>Windows Phone Emulator</li>
<li>Windows Runtime (local)</li>
</ul>
<a name="filtering-kit-settings"></a>
<h2 id="filtering-kit-settings">Filtering Kit Settings</h2>
<p>Typically, only a subset of the kit settings is relevant for a particular setup. Therefore, Qt Creator plugins register sets of relevant settings that you can view and modify in <b>Tools</b> &gt; <b>Options</b> &gt; <b>Kits</b> &gt;. For example, if you use CMake to build all your projects, you can hide Qbs and qmake settings by default.</p>
<p>To hide and show settings in the <b>Kits</b> tab for the current kit, select <b>Settings Filter</b>. To view and modify the settings displayed when you add a new kit, select <b>Default Settings Filter</b>.</p>
<a name="specifying-kit-settings"></a>
<h2 id="specifying-kit-settings">Specifying Kit Settings</h2>
<p>To add kits:</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Kits</b> &gt; <b>Add</b>.<p class="centerAlign"><img src="images/qtcreator-kits.png" alt="" /></p><p>To clone the selected kit, select <b>Clone</b>.</p>
</li>
<li>In the <b>Name</b> column, enter a name for the kit.</li>
<li>Select the <img src="images/qtcreator-desktopdevice-button.png" alt="" /> button to select an image to use as an icon for the kit.</li>
<li>In the <b>File system name</b> field, enter a name for the kit to use as a part of directory names. This value is used for the <i>CurrentKit:FileSystemName</i> variable, which determines the name of the shadow build directory, for example.</li>
<li>In the <b>Device type</b> field, select the type of the device.<p>Double-click the icon next to the field to select the image that is displayed in the kit selector for this kit. You can use any image in a supported file format (for example, PNG). The image is scaled to the size 64x64 pixels. For example, using the compiler logo as an icon allows you to easily see, which compiler is used to build the project for the selected kit.</p>
</li>
<li>In the <b>Device</b> field, select a device.</li>
<li>In the <b>Sysroot</b> field, specify the directory where the device image is located. If you are not cross-compiling, leave this field empty.</li>
<li>In the <b>Emulator skin</b> field, select the skin to use for the <a href="http://doc.qt.io/emulator/index.html">Boot2Qt Emulator Device</a>.</li>
<li>In the <b>Compiler</b> field, select the C or C++ compiler that you use to build the project. You can add compilers to the list if they are installed on the development PC, but were not detected automatically. For more information, see <a href="creator-tool-chains.html">Adding Compilers</a>. This setting is used to tell the code model which compiler is used. If your project type and build tool support it, Qt Creator also tells the build tool to use this compiler for building the project.</li>
<li>In the <b>Environment</b> field, select <b>Change</b> to modify environment variable values for build environments in the <b>Edit Environment Changes</b> dialog. For more information about how to add and remove variable values, see <a href="creator-build-settings.html#batch-editing">Batch Editing</a>.</li>
<li>Select the <b>Force UTF-8 MSVC compiler output</b> check box to either switch the language of MSVC to English or to keep the language setting and just force UTF-8 output, depending on the MSVC compiler used.</li>
<li>In the <b>Debugger</b> field, select the debugger to debug the project on the target platform. Qt Creator automatically detects available debuggers and displays a suitable debugger in the field. You can add debuggers to the list. For more information, see <a href="creator-debuggers.html">Adding Debuggers</a>.<p>For Android kits, the <b>Android GDB server</b> field will display the path to GDB server executable.</p>
</li>
<li>In the <b>Qt version</b> field, select the Qt version to use for building the project. You can add Qt versions to the list if they are installed on the development PC, but were not detected automatically. For more information, see <a href="creator-project-qmake.html">Adding Qt Versions</a>.<p>Qt Creator checks the directories listed in the <code>PATH</code> environment variable for the qmake executable. If a qmake executable is found, it is referred to as <b>Qt in PATH</b> and selected as the Qt version to use for the <b>Desktop</b> kit that is created by default.</p>
</li>
<li>In the <b>Qt mkspec</b> field, specify the name of the mkspec configuration that should be used by qmake. If you leave this field empty, the default mkspec of the selected Qt version is used.</li>
<li>In the <b>Additional Qbs profile settings</b> field, select <b>Change</b> to add settings to Qbs build profiles. For more information, see <a href="creator-targets.html#editing-qbs-profiles">Editing Qbs Profiles</a>.</li>
<li>In the <b>CMake Tool</b> field, select the CMake tool to use for building the project. Select <b>Manage</b> to add installed CMake tools to the list. For more information, see <a href="creator-project-cmake.html#adding-cmake-tools">Adding CMake Tools</a>.</li>
<li>In the <b>CMake generator</b> field, select <b>Change</b> to edit the CMake Generator to use for producing project files. Only the generators with names beginning with the string <b>CodeBlocks</b> produce all the necessary data for the Qt Creator code model. Qt Creator displays a warning if you select a generator that is not supported.</li>
<li>In the <b>CMake configuration</b> field, select <b>Change</b> to edit the variables of the CMake configuration for the kit.</li>
</ol>
<p>Qt Creator uses the <i>default kit</i> if it does not have enough information to choose the kit to use. To set the selected kit as the default kit, select <b>Make Default</b>.</p>
<a name="using-ninja-as-a-cmake-generator"></a>
<h3 id="using-ninja-as-a-cmake-generator">Using Ninja as a CMake Generator</h3>
<p>To use <a href="https://ninja-build.org/">Ninja</a> with CMake, you must install it and select it as the CMake generator in the build and run kit:</p>
<ol class="1" type="1"><li>Install Ninja.</li>
<li>Add the path to the Ninja executable to the value of the PATH system variable.</li>
<li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Kits</b>, and then select the build and run kit for the project.</li>
<li>Select <b>Change</b> next to the <b>CMake generator</b> field to open the <b>CMake Generator</b> dialog.<p class="centerAlign"><img src="images/qtcreator-build-steps-cmake-ninja.png" alt="" /></p></li>
<li>In the <b>Generator</b> field, select <b>Ninja</b>.</li>
<li>Select <b>OK</b> to return to the <b>Kits</b> tab.</li>
<li>Select <b>OK</b> or <b>Apply</b> to save your changes.</li>
</ol>
<p><b>Note: </b>To make sure that old build artifacts don't get in the way the first time you build the project after the change, select <b>Build</b> &gt; <b>Rebuild Project</b>. This cleans up the build directory and performs a new build.</p><a name="editing-qbs-profiles"></a>
<h2 id="editing-qbs-profiles">Editing Qbs Profiles</h2>
<p>To view the Qbs profile associated with the kit, select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Kits</b> &gt; <b>Qbs</b>.</p>
<p>You can add keys and values to the profile or remove them from it, as well as modify existing values. For a list of available keys and values, see <a href="http://doc.qt.io/qbs/list-of-modules.html">List of Modules</a> in the Qbs Manual.</p>
<p>To edit the Qbs profile associated with the kit:</p>
<ol class="1" type="1"><li>Select <b>Change</b> next to the <b>Additional Qbs Profile Settings</b> field to open the <b>Custom Properties</b> dialog.<p class="centerAlign"><img src="images/qtcreator-qbs-profile-settings.png" alt="." /></p></li>
<li>Double-click an empty cell in the <b>Key</b> column to specify the key to add or modify as: <code>&lt;module_name&gt;.&lt;property_name&gt;</code>.</li>
<li>Double-click the cell on the same row in the <b>Value</b> column to specify a value as a JSON literal.</li>
<li>Select <b>Add</b> to add the key-value pair.</li>
<li>Click <b>OK</b>.</li>
</ol>
<p>To modify an existing value, double-click it in the <b>Value</b> field.</p>
<p>To remove the selected property, select <b>Remove</b>.</p>
</div>
<!-- @@@creator-targets.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="creator-configuring-projects.html">Configuring Projects</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-project-qmake.html">Adding Qt Versions</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
