<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- creator-projects-running.qdoc -->
  <title>Running on Multiple Platforms | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Running on Multiple Platforms</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="creator-building-targets.html" />
  <link rel="next" href="creator-deployment.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="creator-building-targets.html">Building for Multiple Platforms</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-deployment.html">Deploying to Devices</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#running-on-embedded-linux-devices">Running on Embedded Linux Devices</a></li>
<li class="level1"><a href="#running-on-qnx-devices">Running on QNX Devices</a></li>
<li class="level2"><a href="#troubleshooting-errors">Troubleshooting Errors</a></li>
<li class="level1"><a href="#running-python-projects">Running Python Projects</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Running on Multiple Platforms</h1>
<span class="subtitle"></span>
<!-- $$$creator-running-targets.html-description -->
<div class="descr"> <a name="details"></a>
<p>By default, running an application also builds it and deploys it to a location from where it can be run on the desktop, on a device emulator or simulator, or on a <a href="creator-glossary.html#glossary-device">device</a> that is connected to the development PC.</p>
<p>To run executable files without deploying them first, select <b>Build</b> &gt; <b>Run Without Deployment</b>. To make this the default option, deselect the <b>Tools</b> &gt; <b>Options</b> &gt; <b>Project</b> &gt; <b>Always deploy project before running it</b> option.</p>
<p>To run applications:</p>
<ol class="1" type="1"><li>Click the <b>Build and Run Kit Selector</b> icon (1) or select <b>Build</b> &gt; <b>Open Build and Run Kit Selector</b> to select the build and run <a href="creator-glossary.html#glossary-buildandrun-kit">kit</a>.<p class="centerAlign"><img src="images/qtcreator-kit-selector.png" alt="&quot;Kit selector&quot;" /></p></li>
<li>Click the <b>Run</b> button (2).</li>
</ol>
<p>The <b>Application Output</b> pane displays the status of the application while it is running. You can select the <b>Run</b> button in the pane to re-run applications without building them first. This is useful when developing Qt Quick applications, because the QML files are interpreted at runtime. Therefore, the application does not need to be built again if you edited only QML files. This saves time especially if the application contains large image files that would need to be bundled into the resource file before running the application.</p>
<p class="centerAlign"><img src="images/qtcreator-application-output.png" alt="" /></p><p>For more information on the options you have, see <a href="creator-run-settings.html">Specifying Run Settings</a>.</p>
<a name="running-on-embedded-linux-devices"></a>
<h2 id="running-on-embedded-linux-devices">Running on Embedded Linux Devices</h2>
<p>To build the application and run it on a device:</p>
<ol class="1" type="1"><li>Specify a connection to the device. For more information, see <a href="creator-developing-generic-linux.html">Connecting Embedded Linux Devices</a>.</li>
<li>Click the <b>Run</b> button.</li>
</ol>
<p>Qt Creator uses the compiler specified in the project build settings (tool chain) to build the application.</p>
<p>Qt Creator copies the application files to the connected device and runs the application. The application views are displayed on the device. Command-line output is visible in the Qt Creator <b>Application Output</b> view.</p>
<p>Choose <b>Projects &gt; Embedded Linux &gt; Run</b> to view the settings for deploying the application to the connected device. For more information, see <a href="creator-run-settings.html#specifying-run-settings-for-linux-based-devices">Specifying Run Settings for Linux-Based Devices</a>. Debugging works transparently if GDB server is installed on the device and it is compatible with the GDB on the host.</p>
<a name="running-on-qnx-devices"></a>
<h2 id="running-on-qnx-devices">Running on QNX Devices</h2>
<ol class="1" type="1"><li>Connect the device to the development PC or to the Wi-Fi network.</li>
<li>Configure the device and specify a connection to it. For more information, see <a href="creator-developing-qnx.html">Connecting QNX Devices</a>.</li>
<li>Make sure that your kit has your QNX device set.</li>
<li>Click the <b>Run</b> button.</li>
</ol>
<p>Qt Creator uses the compiler specified in the QNX tool chain to build the application.</p>
<p><b>Note: </b>Debugging is currently only fully supported on Linux and <a href="nolink">macOS</a>. It is not possible to insert breakpoints during runtime on Windows.</p><a name="troubleshooting-errors"></a>
<h3 id="troubleshooting-errors">Troubleshooting Errors</h3>
<p>To support running, debugging, and stopping applications from Qt Creator, the QNX Neutrino RTOS should provide a few additional command line tools and services, as described in <a href="http://doc.qt.io/qt-5/qnx.html#qt-creator-target-requirements">Qt Creator Target Requirements</a>.</p>
<a name="debug-output-cannot-be-shown"></a>
<h4 id="debug-output-cannot-be-shown">Debug Output Cannot Be Shown</h4>
<p>For the command-line output to show up in the <b>Application Output</b>, Qt Creator needs to be able to establish an SSH connection to the device. This is only possible if QNX Momentics is not running, and the SSH key configured for the device is a 4096-bit key.</p>
<p>If these conditions are not met, you will get an error message saying debug output cannot be shown.</p>
<a name="cannot-run-debug-or-stop-applications"></a>
<h4 id="cannot-run-debug-or-stop-applications">Cannot Run, Debug, or Stop Applications</h4>
<p>The board support package (BSP) for the QNX device might be missing some of the following applications that Qt Creator needs to run, debug, and stop applications on QNX devices: <code>awk</code>, <code>grep</code>, <code>kill</code>, <code>netstat</code>, <code>print</code>, <code>printf</code>, <code>ps</code>, <code>read</code>, <code>sed</code>, <code>sleep</code>, <code>uname</code>, <code>slog2info</code>, and <code>cat</code>.</p>
<p>For more information, see <a href="http://doc.qt.io/qt-5/qnx.html#other-utilities-on-the-target-device">Other Utilities on the Target Device</a>.</p>
<a name="running-python-projects"></a>
<h2 id="running-python-projects">Running Python Projects</h2>
<p>You can execute Qt for Python applications directly from Qt Creator. If you used the <a href="creator-project-creating.html#creating-qt-for-python-applications">new project wizard</a> to create the application project, the <code>main.py</code> file is automatically executed when you select the <b>Run</b> button.</p>
<p>You can specify another file to execute in the <a href="creator-run-settings.html#specifying-run-settings-for-python-projects">run settings</a> of the project.</p>
</div>
<!-- @@@creator-running-targets.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="creator-building-targets.html">Building for Multiple Platforms</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="creator-deployment.html">Deploying to Devices</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
