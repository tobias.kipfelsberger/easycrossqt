<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
<!-- creator-code-indentation.qdoc -->
  <title>Indenting Text or Code | Qt Creator Manual</title>
  <link rel="stylesheet" type="text/css" href="style/offline-simple.css" />
  <script type="text/javascript">
    document.getElementsByTagName("link").item(0).setAttribute("href", "style/offline.css");
    // loading style sheet breaks anchors that were jumped to before
    // so force jumping to anchor again
    setTimeout(function() {
        var anchor = location.hash;
        // need to jump to different anchor first (e.g. none)
        location.hash = "#";
        setTimeout(function() {
            location.hash = anchor;
        }, 0);
    }, 0);
  </script>
</head>
<body>
<div class="header" id="qtdocheader">
  <div class="main">
    <div class="main-rounded">
      <div class="navigationbar">
        <table><tr>
<td ><a href="index.html">Qt Creator Manual</a></td><td >Indenting Text or Code</td></tr></table><table class="buildversion"><tr>
<td id="buildversion" width="100%" align="right"><a href="index.html">Qt Creator Manual 4.11.1</a></td>
        </tr></table>
      </div>
    </div>
<div class="content">
<div class="line">
<div class="content mainContent">
  <link rel="prev" href="creator-completing-code.html" />
  <link rel="next" href="qt-quick-toolbars.html" />
<p class="naviNextPrevious headerNavi">
<a class="prevPage" href="creator-completing-code.html">Completing Code</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="qt-quick-toolbars.html">Using Qt Quick Toolbars</a>
</p><p/>
<div class="sidebar">
<div class="toc">
<h3><a name="toc">Contents</a></h3>
<ul>
<li class="level1"><a href="#indenting-c-files">Indenting C++ Files</a></li>
<li class="level2"><a href="#automatic-formatting-and-indentation">Automatic Formatting and Indentation</a></li>
<li class="level1"><a href="#indenting-qml-files">Indenting QML Files</a></li>
<li class="level1"><a href="#indenting-nim-files">Indenting Nim Files</a></li>
<li class="level1"><a href="#indenting-other-text-files">Indenting Other Text Files</a></li>
<li class="level1"><a href="#specifying-tab-settings">Specifying Tab Settings</a></li>
<li class="level2"><a href="#specifying-tabs-and-indentation">Specifying Tabs and Indentation</a></li>
<li class="level1"><a href="#specifying-typing-options">Specifying Typing Options</a></li>
<li class="level1"><a href="#specifying-settings-for-content">Specifying Settings for Content</a></li>
<li class="level1"><a href="#specifying-settings-for-braces">Specifying Settings for Braces</a></li>
<li class="level1"><a href="#specifying-settings-for-switch-statements">Specifying Settings for Switch Statements</a></li>
<li class="level1"><a href="#specifying-alignment">Specifying Alignment</a></li>
<li class="level1"><a href="#binding-pointers-and-references">Binding Pointers and References</a></li>
<li class="level1"><a href="#naming-getters">Naming Getters</a></li>
</ul>
</div>
<div class="sidebar-content" id="sidebar-content"></div></div>
<h1 class="title">Indenting Text or Code</h1>
<span class="subtitle"></span>
<!-- $$$creator-indenting-code.html-description -->
<div class="descr"> <a name="details"></a>
<p>When you type text or code, it is indented automatically according to the selected text editor or code style options. Select a block to indent it when you press <b>Tab</b>. Press <b>Shift+Tab</b> to decrease the indentation. You can disable automatic indentation.</p>
<p>You can specify indentation for:</p>
<ul>
<li>C++ files</li>
<li>QML files</li>
<li>Nim files</li>
<li>Other text files</li>
</ul>
<p>You can also specify indentation separately for each project. You can specify several sets of code style settings and easily switch between them. In addition, you can import and export code style settings.</p>
<a name="indenting-c-files"></a>
<h2 id="indenting-c-files">Indenting C++ Files</h2>
<p>To specify indentation settings for the C++ editor:</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>C++</b>.</li>
<li>In the <b>Current settings</b> field, select the settings to modify and click <b>Copy</b>.<p class="centerAlign"><img src="images/qtcreator-options-code-style-cpp.png" alt="&quot;C++ Code Style options&quot;" /></p></li>
<li>Give a name to the settings and click <b>OK</b>.</li>
<li>Click <b>Edit</b> to specify code style settings for the project.<p class="centerAlign"><img src="images/qtcreator-code-style-settings-edit-cpp.png" alt="&quot;Edit Code Style Settings dialog&quot;" /></p></li>
</ol>
<p>You can specify how to:</p>
<ul>
<li>Interpret the <b>Tab</b> and <b>Backspace</b> key presses.</li>
<li>Indent the contents of classes, functions, blocks, and namespaces.</li>
<li>Indent braces in classes, namespaces, enums, functions, and blocks.</li>
<li>Control switch statements and their contents.</li>
<li>Align continuation lines.</li>
<li>Bind pointers (*) and references (&amp;) in types and declarations to identifiers, type names, or left or right <code>const</code> or <code>volatile</code> keywords.</li>
<li>Name getter functions.</li>
</ul>
<p>You can use the live preview to see how the options change the indentation.</p>
<p>To specify different settings for a particular project, select <b>Projects</b> &gt; <b>Code Style Settings</b>.</p>
<a name="automatic-formatting-and-indentation"></a>
<h3 id="automatic-formatting-and-indentation">Automatic Formatting and Indentation</h3>
<p>The Clang Format plugin uses the <a href="https://clang.llvm.org/docs/LibFormat.html">LibFormat</a> library for automatic formatting and indentation.</p>
<p>To enable the plugin, select <b>Help</b> &gt; <b>About Plugins</b> &gt; <b>C++</b> &gt; <b>ClangFormat</b>. Then restart Qt Creator to load the plugin.</p>
<p><b>Note: </b>If you enable the plugin, do not use the <a href="creator-beautifier.html">Beautifier</a>, because combining the two can provide unexpected results.</p><p>You can use Clang Format to enforce a coding style for a project or the whole organization. Create a <code>.clang-format</code> file that contains the <a href="https://clang.llvm.org/docs/ClangFormatStyleOptions.html">Clang Format Style Options</a> to use and save it in the root folder of the project or one of its parent folders. The plugin searches for the Clang format file recursively from the directory that contains the source file up to the file system root.</p>
<p>To specify settings for automatic formatting and indentation:</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>C++</b>.<p class="centerAlign"><img src="images/qtcreator-clang-format-options.png" alt="&quot;C++ Clang Format options&quot;" /></p></li>
<li>Select the <b>Format instead of indenting</b> check box to use the <b>Ctrl+I</b> keyboard shortcut to format code instead of indenting it.</li>
<li>Select the <b>Format while typing</b> check box to apply the formatting while you type code.</li>
<li>Select the <b>Format edited code on file save</b> check box to apply the formatting to the edited code when you save the file.</li>
<li>Select the <b>Override Clang Format configuration file</b> check box to create a local configuration file that overrides the one stored in the file system.<p><b>Note: </b>This is not recommended, because it defeats the purpose of a Clang format file.</p></li>
<li>To modify the values in the file, select them in the left-side column and enter the new values.</li>
<li>To view examples of the new values in the right-hand column, select <b>Apply</b>.</li>
</ol>
<p>To override the <code>.clang-format</code> file for a project, select <b>Projects</b> &gt; <b>Project Settings</b> &gt; <b>Code Style</b> &gt; <b>Override Clang Format configuration file</b>.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-clang-format.png" alt="" /></p><p>You can create <code>.clang-format</code> files that contain the configuration options of a certain predefined style from the command line. For example, to create a format file for the LLVM style, enter the following command:</p>
<pre class="cpp plain">

  clang-format -style=llvm -dump-config &gt; .clang-format

</pre>
<a name="indenting-qml-files"></a>
<h2 id="indenting-qml-files">Indenting QML Files</h2>
<p>To specify settings for the Qt Quick editor:</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Qt Quick</b>.</li>
<li>In the <b>Current settings</b> field, select the settings to modify and click <b>Copy</b>.<p class="centerAlign"><img src="images/qtcreator-options-code-style-qml.png" alt="&quot;QML Code Style options&quot;" /></p></li>
<li>Give a name to the settings and click <b>OK</b>.</li>
<li>Click <b>Edit</b> to specify code style settings for the project.<p class="centerAlign"><img src="images/qtcreator-code-style-settings-edit-qtquick.png" alt="&quot;Edit Code Style Settings dialog&quot;" /></p></li>
</ol>
<p>You can specify how to interpret the <b>Tab</b> key presses and how to align continuation lines.</p>
<p>To specify different settings for a particular project, select <b>Projects</b> &gt; <b>Code Style Settings</b>.</p>
<a name="indenting-nim-files"></a>
<h2 id="indenting-nim-files">Indenting Nim Files</h2>
<p>To specify settings for the Nim editor (experimental):</p>
<ol class="1" type="1"><li>Select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Nim</b>.</li>
<li>In the <b>Current settings</b> field, select the settings to modify and click <b>Copy</b>.<p class="centerAlign"><img src="images/qtcreator-options-code-style-nim.png" alt="&quot;Nim Code Style options&quot;" /></p></li>
<li>Give a name to the settings and click <b>OK</b>.</li>
<li>Click <b>Edit</b> to specify code style settings for the project.<p class="centerAlign"><img src="images/qtcreator-code-style-settings-edit-nim.png" alt="&quot;Edit Code Style Settings dialog&quot;" /></p></li>
</ol>
<p>You can specify how to interpret the <b>Tab</b> key presses and how to align continuation lines.</p>
<p>To specify different settings for a particular project, select <b>Projects</b> &gt; <b>Code Style</b>.</p>
<a name="indenting-other-text-files"></a>
<h2 id="indenting-other-text-files">Indenting Other Text Files</h2>
<p>To specify indentation settings for text files that do not contain C++ or QML code (such as Python code files), select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Text Editor</b> &gt; <b>Behavior</b>.</p>
<p class="centerAlign"><img src="images/qtcreator-indentation.png" alt="&quot;Text Editor Behavior options&quot;" /></p><p>To specify different settings for a particular project, select <b>Projects</b> &gt; <b>Editor</b>.</p>
<p>You can specify how to interpret the <b>Tab</b> and <b>Backspace</b> key presses and how to align continuation lines.</p>
<a name="specifying-tab-settings"></a>
<h2 id="specifying-tab-settings">Specifying Tab Settings</h2>
<p>You can specify tab settings at the following levels:</p>
<ul>
<li>For all C++ files</li>
<li>For all QML files</li>
<li>For all other text files</li>
<li>For C++ files in a project</li>
<li>For QML files in a project</li>
<li>For other text files in a project</li>
</ul>
<a name="specifying-tabs-and-indentation"></a>
<h3 id="specifying-tabs-and-indentation">Specifying Tabs and Indentation</h3>
<p>You can specify tab policy and tab size in the <b>Tabs and Indentation</b> group. In the <b>Tab policy</b> field, select whether to use only spaces or only tabs for indentation, or to use a mixture of them.</p>
<p>By default, the tab length in code editor is 8 spaces and the indent size is 4 spaces. You can specify the tab length and indent size separately for each project and for different types of files.</p>
<p>You can have continuation lines aligned with the previous line. In the <b>Align continuation lines</b> field, select <b>Not at all</b> to disable automatic alignment and indent continuation lines to the logical depth. To always use spaces for alignment, select <b>With Spaces</b>. To follow the <b>Tab policy</b>, select <b>With Regular Indent</b>.</p>
<a name="specifying-typing-options"></a>
<h2 id="specifying-typing-options">Specifying Typing Options</h2>
<p>When you type text or code, it is indented automatically according to the selected text editor or code style options. To specify typing options, select <b>Tools</b> &gt; <b>Options</b> &gt; <b>Text Editor</b> &gt; <b>Behavior</b> &gt; <b>Typing</b>.</p>
<p>To disable automatic indentation, deselect the <b>Enable automatic indentation</b> check box.</p>
<p>You can specify how the indentation is decreased when you press <b>Backspace</b> in the <b>Backspace indentation</b> field. To go back one space at a time, select <b>None</b>. To decrease indentation in leading white space by one level, select <b>Follows Previous Indents</b>. To move back one tab length if the character to the left of the cursor is a space, select <b>Unindents</b>.</p>
<p>You can specify whether the <b>Tab</b> key automatically indents text when you press it. To automatically indent text, select <b>Always</b> in the <b>Tab key performs auto-indent</b> field. To only indent text when the cursor is located within leading white space, select <b>In Leading White Space</b>.</p>
<a name="specifying-settings-for-content"></a>
<h2 id="specifying-settings-for-content">Specifying Settings for Content</h2>
<p>You can indent public, protected, and private statements and declarations related to them within classes.</p>
<p>You can also indent statements within functions and blocks and declarations within namespaces.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-content.png" alt="&quot;Content options&quot;" /></p><a name="specifying-settings-for-braces"></a>
<h2 id="specifying-settings-for-braces">Specifying Settings for Braces</h2>
<p>You can indent class, namespace, enum and function declarations and code blocks.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-braces.png" alt="&quot;Braces options&quot;" /></p><a name="specifying-settings-for-switch-statements"></a>
<h2 id="specifying-settings-for-switch-statements">Specifying Settings for Switch Statements</h2>
<p>You can indent case or default statements, or statements or blocks related to them within switch statements.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-switch.png" alt="&quot;Switch options&quot;" /></p><a name="specifying-alignment"></a>
<h2 id="specifying-alignment">Specifying Alignment</h2>
<p>To align continuation lines to tokens after assignments, such as <code>=</code> or <code>+=</code>, select the <b>Align after assignments</b> check box. You can specify additional settings for aligning continuation lines in the <b>General</b> tab.</p>
<p>You can also add spaces to conditional statements, so that they are not aligned with the following line. Usually, this only affects <code>if</code> statements.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-alignment.png" alt="&quot;Alignment options&quot;" /></p><a name="binding-pointers-and-references"></a>
<h2 id="binding-pointers-and-references">Binding Pointers and References</h2>
<p>To bind pointers (<code>*</code>) and references (<code>&amp;</code>) in types and declarations to identifiers, type names, or left or right <code>const</code> or <code>volatile</code> keywords, select the check boxes in the <b>Pointers and References</b> tab.</p>
<p>The <code>*</code> and <code>&amp;</code> characters are automatically bound to identifiers of pointers to functions and pointers to arrays.</p>
<p class="centerAlign"><img src="images/qtcreator-pointers-references.png" alt="&quot;Pointers and References options&quot;" /></p><a name="naming-getters"></a>
<h2 id="naming-getters">Naming Getters</h2>
<p>To prefer getter names without the string <i>get</i>, select the <b>Prefer getter names without &quot;get&quot;</b> check box in the <b>Getter and Setter</b> tab.</p>
<p class="centerAlign"><img src="images/qtcreator-code-style-getter-setter.png" alt="" /></p></div>
<!-- @@@creator-indenting-code.html -->
<p class="naviNextPrevious footerNavi">
<a class="prevPage" href="creator-completing-code.html">Completing Code</a>
<span class="naviSeparator">  &#9702;  </span>
<a class="nextPage" href="qt-quick-toolbars.html">Using Qt Quick Toolbars</a>
</p>
        </div>
       </div>
   </div>
   </div>
</div>
<div class="footer">
   <p>
   <acronym title="Copyright">&copy;</acronym> 2019 The Qt Company Ltd.
   Documentation contributions included herein are the copyrights of
   their respective owners.<br>    The documentation provided herein is licensed under the terms of the    <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation    License version 1.3</a> as published by the Free Software Foundation.<br>    Qt and respective logos are trademarks of The Qt Company Ltd.     in Finland and/or other countries worldwide. All other trademarks are property
   of their respective owners. </p>
</div>
</body>
</html>
