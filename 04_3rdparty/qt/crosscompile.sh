#!/bin/bash

# some notes:
# 1) hostprefix must be inside target prefix, otherwise the generated cmake files will contain absolute pathes
# 2) the makefiles produce errors for me; My guess is, that parallel build does not really work. Solution around this: fix the error and call the script again!
#    the most common errors:  
#      - PCH (precompiled-headers) related errors: rm the '.pch' folder where the error occured
#      - some static library cannot be found: find folder where this library is being created, and call the makefile there manually

basedir=`dirname "$(realpath $0)"`

## download & crosscompile Qt 5.14.1
QT_MAJOR=5
QT_MINOR=14
QT_PATCH=1

PROJECT_ROOT=$basedir/../..

# folder for extracting and building
TMP_FOLDER="$basedir/tmp"

#install prefix
INSTALL_FOLDER_HOST=$basedir/install
INSTALL_FOLDER_TARGET=/opt/qt5

#crosscompile prefix & sysroot
CROSSCOMPILE_PREFIX=arm-linux-gnueabihf-
TOOLCHAINROOT=$PROJECT_ROOT/02_toolchain/cross-pi-gcc/bin
SYSROOT=$PROJECT_ROOT/03_sysroot

#parallel build
NUM_PARALLEL_BUILDS=`nproc`

SOURCE_FOLDER=$TMP_FOLDER/source
LOG_FOLDER=$TMP_FOLDER/logs
DOWNLOADS_FOLDER=$TMP_FOLDER/downloads

# setup pathes
PATH=$TOOLCHAINROOT/bin:$PATH
LD_LIBRARY_PATH=$TOOLCHAINROOT/lib:$LD_LIBRARY_PATH 
LD_LIBRARY_PATH=$TOOLCHAINROOT/arm-linux-gnueabihf/lib:$LD_LIBRARY_PATH 
LD_LIBRARY_PATH=$TOOLCHAINROOT/arm-linux-gnueabihf/libc/usr/lib:$LD_LIBRARY_PATH 
LD_LIBRARY_PATH=$TOOLCHAINROOT/arm-linux-gnueabihf/libc/lib:$LD_LIBRARY_PATH 

# break on error
set -e

#download Qt
mkdir -p $DOWNLOADS_FOLDER
cd $DOWNLOADS_FOLDER
wget -nc https://download.qt.io/official_releases/qt/$QT_MAJOR.$QT_MINOR/$QT_MAJOR.$QT_MINOR.$QT_PATCH/single/qt-everywhere-src-$QT_MAJOR.$QT_MINOR.$QT_PATCH.tar.xz

#extract Qt
if [ ! -d $SOURCE_FOLDER ]; then
    mkdir -p $SOURCE_FOLDER
    tar xvf qt-everywhere-src-$QT_MAJOR.$QT_MINOR.$QT_PATCH.tar.xz -C $SOURCE_FOLDER --strip-components=1
fi

#create log folder
mkdir -p $LOG_FOLDER

# configure Qt
cd $SOURCE_FOLDER
./configure --help > $LOG_FOLDER/configure-options.txt
./configure \
	-release \
	-opengl es2 \
	-device linux-rasp-pi-g++ \
	-device-option CROSS_COMPILE=$TOOLCHAINROOT/$CROSSCOMPILE_PREFIX \
	-sysroot $SYSROOT \
	-opensource \
    -feature-relocatable \
    -c++std c++17 \
    -silent \
	-confirm-license \
	-skip qtwayland \
	-skip qtlocation \
	-skip qtscript \
	-make libs \
	-prefix $INSTALL_FOLDER_TARGET \
	-extprefix ../../install \
	-hostprefix ../../install/host\
	-no-use-gold-linker \
    -no-gcc-sysroot \
	-no-gbm 2>&1 | tee $LOG_FOLDER/configure-log.txt

make -j $NUM_PARALLEL_BUILDS 2>&1 | tee $LOG_FOLDER/make-log.txt
make install | tee $LOG_FOLDER/make-install-log.txt

